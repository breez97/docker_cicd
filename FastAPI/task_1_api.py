"""Import necessary libraries"""
from random import randint
from fastapi import FastAPI
from uvicorn import run
import redis


LENGTH = 100
app = FastAPI(title='Lab_1_Task')
redis_connection = redis.Redis(host='localhost', port=6379, db=0)


def check_and_generate_array():
    """Function checks if array already exists in redis"""
    if redis_connection.exists('array'):
        array_str = redis_connection.get('array')
        array = [int(num) for num in array_str.decode().split(',')]
    else:
        array = [randint(0, 100) for _ in range(LENGTH)]
        array.sort()
        array_str = ','.join(map(str, array))
        redis_connection.set('array', array_str)
    return array


def binary_search(array, number):
    """Function makes binary search of an array"""
    index, left, right = -1, 0, len(array) - 1
    while left <= right:
        middle = (left + right) // 2
        if number == array[middle]:
            index = middle
            break
        if number > array[middle]:
            left = middle + 1
        else:
            right = middle - 1
    return index


@app.get("/search/{number}")
def search_number(number: int):
    """Function search index of number in array"""
    array = check_and_generate_array()
    index = binary_search(array, number)
    return {"Index of value": index}


if __name__ == '__main__':
    run(app, host='localhost', port=8080)

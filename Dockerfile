# До модернизации

# FROM python:3
# WORKDIR /app
# COPY task_1.py ./
# ENTRYPOINT ["python3", "task_1.py"]
# CMD $1


# После модернизации

FROM python:3
WORKDIR /app
COPY /FastAPI /app
RUN pip install -r ./requirements.txt
ENTRYPOINT ["python3", "task_1_api.py"]
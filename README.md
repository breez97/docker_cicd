# Лабораторная 1 <br> Docker + Gitlab Ci/CD

## Цель: обучиться основам контейнеризации, а также процессу CI/CD

### Содержание:
1. [**Загрузить образ веб-сервера `nginx` и запустить его с параметрами**](#задание-1)
2. [**Написать программу на любом языке программирования, которая выполняет  задачу поиска требуемого значения в массиве случайных чисел.**](#задание-2)
3. [**Настроить собственный `gitlab-runner` для проекта и написать сценарий .`gitlab-ci.yml`.**](#задание-3)
4. [**Модернизировать приложение таким образом, чтобы оно было частью бэкенд сервиса.**](#задание-4)

## Задание 1:

Загрузить образ веб-сервера `nginx` и запустить его с параметрами:

- входной порт “год_вашего_рождения:80”
- фоновое исполнение

**Запуск:**

```
docker run -p 2003:80 -d nginx
```

Результатом должен быть скриншот части окна браузера со стандартным приветствием `nginx` по адресу `http://localhost:2003`. На скриншоте должны быть видны приветствие и адресная строка.

### Результат:

![](./misc/Task_1/nginx_terminal.png)

![](./misc/Task_1/nginx_browser.png)

## Задание 2:

### Часть 1:

Написать программу на любом языке программирования, которая выполняет  задачу поиска требуемого значения в массиве случайных чисел. Требования к программе:
- Массив генерируется случайно при запуске приложения.
- Массив должен быть отсортирован по порядку.
- Выполнять поиск с помощью бинарного алгоритма.
- Размер массива - 100 чисел.
- Пользователь при запуске программы вводит искомое значение.
- Значение передаётся через параметр командной строки.

В качестве искомого значения требуется ввести сумму цифр в дате рождения студента, умноженную на два, например, `01.01.2003 = 1+1+2+3 = 7 * 2 = 14.`

**Запуск:** `14.08.2003 -> 1 + 4 + 8 + 2 + 3 = 18`
```
python3 task_1.py 18
```
### Часть 2:

С помощью `dockerfile` создать образ приложения.

Результатами выполнения задания будут:

- Листинг `dockerfile`
- Скриншот выполнения программы при помощи `docker run`

**Важно**! Переменная искомого значения должна передаваться **при запуске контейнера**, а не “хардкодом” внутри приложения или `dockerfile`.

### Результат:

#### Листинг `Dockerfile`

```
FROM python:3
WORKDIR /app
COPY task_1.py ./
ENTRYPOINT ["python3", "task_1.py"]
CMD $1
```

#### Скриншот выполнения программы при помощи `docker run`

![](./misc/Task_2/app_image_build.png)

![](./misc/Task_2/app_test.png)

## Задание 3:

Найти способ воспользоваться сервисом `GitLab`. Разрешается локально развернуть сервис для себя или группы с помощью удалённого хостинга. Можно попробовать зарегистрироваться через Google аккаунт.

При успешном получении доступа к сервису, необходимо создать репозиторий для проекта из задания 2 и загрузить всю исходную базу.

Далее, согласно [гайду](https://docs.gitlab.com/ee/ci/), настроить собственный `gitlab-runner` для проекта и написать сценарий `.gitlab-ci.yml`, содержащий две стадии:

1. Сборка приложения (любой линтер, если это скриптовой язык)
2. Тестирование приложения

В качестве параметра искомого значения для тестирования приложения использовать переменные окружения самого `GitLab` (то есть, без хардкода в сценарии).

Результатами выполнения задания будут:

- Ссылка на репозиторий в `GitLab`.
- Листинг `.gitlab-ci.yml`.
- Скриншот выполнения пайплайна.

### Результат:

#### <a href="https://gitlab.com/breez97/docker_cicd">Ссылка на репозиторий в `GitLab`</a>

#### Листинг `.gitlab-ci.yml`

```
image: python:3

stages:
  - build
  - lint
  - test

build-job:
  stage: build
  script:
    - echo "Project build"

lint-job:
  stage: lint
  script:
    - pip3 install pylint
    - python3 -m pylint task_1.py
  

test-job:
  stage: test
  script:
    - python3 task_1.py $ENV_TEST_VALUE_12
    - python3 task_1.py $ENV_TEST_VALUE_2
    - python3 task_1.py $ENV_TEST_VALUE_43
    - python3 task_1.py $ENV_TEST_VALUE_76
    - python3 task_1.py $ENV_TEST_VALUE_97
```

#### Скриншот выполнения пайплайна

**Пайплайн приложения**

![](./misc/Task_3/pipeline.png)

**Сборка приложения**

![](./misc/Task_3/pipeline_build_job.png)

**Линтер**

![](./misc/Task_3/pipeline_lint_job_1.png)
![](./misc/Task_3/pipeline_lint_job_2.png)

**Тестирование приложения**

![](./misc/Task_3/pipeline_test_job.png)

## Задание 4:

Модернизировать приложение таким образом, чтобы оно было частью бэкенд сервиса – для этого можно использовать любой фреймворк `(FastAPI, ASP.NET, Django, Spring Boot, etc…).`

Требования следующие:

- Пользователь отправляет на энд-поинт сервиса один запрос, содержащий искомое число.
- Сервер возвращает либо индекс в массиве, где это число расположено, либо -1, если его нет.
- При **первом запуске сервера** массив генерируется с нуля.
- Значения массива сохраняются в базу данных redis и загружаются оттуда при последующих перезапусках сервера.
- Приложение принимает запросы на порт `8080`.

Иными словами, необходимо написать `docker-compose.yml` файл, который будет содержать в себе описания `сборки и запуска приложения (app)`, `сборки и запуска базы данных (redis)`.

Внести в сценарий `.gitlab-ci.yml` изменения таким образом, чтобы приложение поднималось в контейнере на хостинге, где расположен `gitlab-runner` (как правило, это может быть локальный компьютер).

Результатами работы будут:

1. Листинг `docker-compose.yml`
2. Листинг `.gitlab-ci.yml`
3. Скриншот выполнения пайплайна.
4. Скриншоты тестирования энд-поинта с помощью `Postman\Insomnia`

### Результат:

#### Листинг `docker-compose.yml`

```
version: '3'

services:
  app:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8080:8080"
    depends_on:
      - redis

  redis:
    image: "redis:latest"
    ports:
      - "6379:6379"
```

#### Листинг `.gitlab-ci.yml`

```
stages:
  - build

build-job:
  stage: build
  services:
    - docker:dind
    - redis:latest
    - python:3
  stage: build
  before_script:
    - docker-compose --version
  script:
    - docker-compose build
    - docker-compose up
```

#### Скриншот выполнения пайплайна

![](./misc/Task_4/pipeline_task_4.png)

#### Скриншоты тестирования энд-поинта с помощью `Postman\Insomnia`

![](./misc/Task_4/postman_test_1.png)
![](./misc/Task_4/postman_test_2.png)
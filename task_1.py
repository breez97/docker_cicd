"""Import necessary libraries"""
import sys
from random import randint

LENGTH = 100

def is_int(input_string):
    """Function defines is input str can be converted into int"""
    try:
        int(input_string)
        return True
    except ValueError:
        return False


def binary_search(array, number):
    """Function makes binary search of an array"""
    index, left, right = -1, 0, len(array) - 1
    while left <= right:
        middle = (left + right) // 2
        if number == array[middle]:
            index = middle
            break
        if number > array[middle]:
            left = middle + 1
        else:
            right = middle - 1
    if index == -1:
        print('No such element in array')
    else:
        print(f'Index of value: {index}')

def program():
    """Main program"""
    array = [randint(0, 100) for _ in range(LENGTH)]
    array.sort()
    if len(sys.argv) == 2:
        if is_int(sys.argv[1]):
            number = int(sys.argv[1])
            binary_search(array, number)
        else:
            print('Variable is not int number\nUsage: python3 <filename> <number>')
    else:
        print('Incorrect launch\nUsage: python3 <filename> <number>')


if __name__ == '__main__':
    program()
